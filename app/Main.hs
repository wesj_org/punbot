{-# LANGUAGE OverloadedStrings #-}

module Main where

import Slack
import Slack.Websocket
import Slack.API
import Slack.Types

import Puns

import qualified Logging as Log
import qualified Data.Text as T

import qualified System.Environment as Env

apiFromEnv :: IO SlackAPIClient
apiFromEnv = do
    slack_app_key <- Env.getEnv "SLACK_APP_TOKEN"
    slack_bot_token <- Env.getEnv "SLACK_BOT_USER_TOKEN"
    let api = client (T.pack slack_app_key) (T.pack slack_bot_token)
    return api

punDbFromEnv :: IO PunDB
punDbFromEnv = (Env.getEnv "FORTUNES_FILE") >>= Puns.createDb

wsUrlFromEnv :: IO WebSocketUrl
wsUrlFromEnv = apiFromEnv >>= appsConnectionsOpen

---

_pun :: PunDB -> MessageHandler
_pun db msg api = do
    print $ show msg
    Log.info "!pun handler: %s\n" (show msg)
    pun <- getPunIO db
    resp <- chatPostMessage (sm_channel msg) (punAsText pun) api -- todo: better error handling
    Log.info "slack api response: %s\n" (show resp)
    return (Right () :: Either String ())

_puninfo :: PunDB -> MessageHandler
_puninfo db msg api = do
    Log.info "!puninfo handler: %s\n" (show msg)
    let response_txt = T.pack $ "Database contains " ++ show (Puns.size db) ++ " puns."
    resp <- chatPostMessage (sm_channel msg) response_txt api -- todo: better error handling
    Log.info "slack api response: %s\n" (show resp)
    return (Right () :: Either String ())

_messageFilters :: PunDB -> [MessageFilter]
_messageFilters puns = [
    Slack.filter "!pun" (_pun puns),
    Slack.filter "!puninfo" (_puninfo puns)
    ]

---

main :: IO ()
main = do
    api <- apiFromEnv
    db <- punDbFromEnv

    let bot = messageBot api (_messageFilters db)

    runMessageBot bot