{-# LANGUAGE OverloadedStrings #-}

module Slack where

import Slack.Types
import Slack.API
import Slack.Websocket

import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL

import Debug.Trace
import Control.Monad

type MessageHandler = SlackMessage -> SlackAPIClient -> IO (Either String ())

data MessageFilter = MessageFilter T.Text MessageHandler

data SlackMessageBot = SlackMessageBot {
    _smb_api :: SlackAPIClient,
    _smb_filters :: [MessageFilter]
}

instance MessageReciver SlackMessageBot where
    handleMessage bot msg = do
        ex <- sequence [ _execFilter bot filter msg | filter <- _smb_filters bot, _filterDoesMatch filter msg ]
        let err = foldr (_joinLeft "\n") "" ex
        case err of
            "" -> return (Right ())
            s -> return (Left s)

---

_joinLeft :: String -> Either String b -> String -> String
_joinLeft _ (Right _) s  = s
_joinLeft j (Left e) s = s ++ j ++ e

filter :: T.Text -> MessageHandler -> MessageFilter
filter = MessageFilter

messageBot :: SlackAPIClient -> [MessageFilter] -> SlackMessageBot
messageBot = SlackMessageBot

runMessageBot :: SlackMessageBot -> IO ()
runMessageBot bot = do
    wsurl <- appsConnectionsOpen (_smb_api bot)
    let client = messageReciverClient bot
    runClient client wsurl

_execFilter :: SlackMessageBot -> MessageFilter -> SlackMessage -> IO (Either String ())
_execFilter bot (MessageFilter _ handler) message = handler message (_smb_api bot)

_filterDoesMatch :: MessageFilter -> SlackMessage -> Bool
_filterDoesMatch (MessageFilter filter _) msg = T.isInfixOf filter (sm_text msg)