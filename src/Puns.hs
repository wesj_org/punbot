module Puns where

import qualified System.Random as R
import qualified Data.Vector as V
import qualified Data.Text as T
import Debug.Trace (trace)
import Data.List

newtype Pun = Pun String deriving (Show)

newtype PunDB = PunDB (V.Vector Pun)

makePun :: String -> Pun
makePun = Pun

size :: PunDB -> Int
size (PunDB v) = V.length v

getPunUnsafe :: PunDB -> Int -> Pun
getPunUnsafe (PunDB v) = V.unsafeIndex v

getPun :: R.RandomGen g => PunDB -> g -> (Pun, g)
getPun db rng = let (i, rng') = R.randomR (0, size db) rng
    in (getPunUnsafe db i, rng')

getPunIO :: PunDB -> IO Pun
getPunIO db = do
    i <- R.randomRIO (0, size db)
    return $ getPunUnsafe db i

_munchPuns :: [Pun] -> String -> [Pun]
_munchPuns puns [] = puns
_munchPuns puns buf = let
    (pun, rest) = span (/='%') buf
    puns_next = makePun pun : puns
    in case rest of
        [] -> puns_next
        other -> _munchPuns puns_next (tail other)

createDb :: FilePath -> IO PunDB
createDb path = do
    file <- readFile path
    let puns = _munchPuns [] file
    return $ PunDB (V.fromList puns)

punAsText :: Pun -> T.Text
punAsText (Pun s) = T.pack s