module Logging where
    
    import Text.Printf

    trace :: PrintfType r => String -> r
    trace pf = printf ("[TRACE] " ++ pf)

    debug :: PrintfType r => String -> r
    debug pf = printf ("[DEBUG] " ++ pf)

    info :: PrintfType r => String -> r
    info pf = printf ("[INFO] " ++ pf)

    warn :: PrintfType r => String -> r
    warn pf = printf ("[WARNING] " ++ pf)

    err :: PrintfType r => String -> r
    err pf = printf ("[ERROR] " ++ pf)