{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
module Slack.API where

import Data.Aeson
import Data.Aeson.Types
import Data.List

import Network.HTTP.Req

import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL

import qualified Logging as Log

import Debug.Trace (trace)

data SlackAPIClient = SlackAPIClient {
    _app :: T.Text,
    _bot :: T.Text 
}

type SlackAPIReq a = SlackAPIClient -> IO a

data WebSocketUrl = WebSocketUrl { ws_host :: String, ws_path :: String, ws_port :: Int } 
    deriving (Show)

instance FromJSON WebSocketUrl where
    parseJSON = withObject "WebSocketUrl" $ \o -> do
        ok <- o .: "ok"
        case ok of
            False -> do
                err <- o .: "error"
                fail err
            True -> do
                url <- (trace (show o) o) .: "url"

                let parsed = do
                        base <- stripPrefix "wss://" url `orElse` "url missing wss:// prefix"
                        let (host, path) = span (/='/') base
                        return (WebSocketUrl host path 443)

                case parsed of
                    Left err -> fail err
                    Right ok -> return ok

---

client :: T.Text -> T.Text -> SlackAPIClient
client = SlackAPIClient

apiAppAuth :: SlackAPIClient -> B.ByteString 
apiAppAuth = TE.encodeUtf8 . _app

apiBotAuth :: SlackAPIClient -> B.ByteString 
apiBotAuth = TE.encodeUtf8 . _bot

appsConnectionsOpen :: SlackAPIReq WebSocketUrl
appsConnectionsOpen = _doAppRequest "apps.connections.open"

chatPostMessage :: T.Text -> T.Text -> SlackAPIReq Value
chatPostMessage channelId text = let opts = queryParam "channel" (Just channelId) <> queryParam "text" (Just text)
    in _doRequest "chat.postMessage" opts

orElse :: Maybe b -> a -> Either a b
orElse (Just n) _ = Right n
orElse Nothing x = Left x

---

_doAppRequest :: (FromJSON b) => T.Text -> SlackAPIClient -> IO b
_doAppRequest method cli = runReq _slackHttpConfig $ do
    let opts = oAuth2Bearer (apiAppAuth cli) <> header "Content-type" "application/x-www-form-urlencoded"
    let url = _slackUrl method

    r <- req POST url
        NoReqBody
        jsonResponse
        opts

    return $ responseBody r

_doRequest :: (FromJSON b) => T.Text -> Option Https -> SlackAPIClient -> IO b
_doRequest method params cli = runReq _slackHttpConfig $ do
    let opts = oAuth2Bearer (apiBotAuth cli) <> header "Content-type" "application/x-www-form-urlencoded" <> params
    let url = _slackUrl method

    r <- req POST url
        NoReqBody
        jsonResponse
        opts

    return $ responseBody r

_slackHttpConfig :: HttpConfig 
_slackHttpConfig = defaultHttpConfig 

_slackUrl :: T.Text -> Url Https
_slackUrl = (https "slack.com" /: "api" /:)