{-# LANGUAGE OverloadedStrings #-}

module Slack.Websocket where

    import Slack.Types
    import Slack.API

    import qualified Data.ByteString as B
    import qualified Data.ByteString.Lazy as BL
    import qualified Data.Text as T

    import Data.Aeson
    import Data.Aeson.Types

    import Wuss
    import qualified Network.WebSockets as WS
    import Network.Socket 

    import Data.List

    import Control.Exception (bracket_)

    import qualified Logging as Log

---

    -- todo ignore retries

    data WebSocketMessage = WebSocketMessage { wsm_type :: T.Text, wsm_envelope_id :: Maybe T.Text, wsm_payload :: Maybe Value, wsm_debug_info :: Maybe Value }
        deriving (Show)

    instance FromJSON WebSocketMessage where
        parseJSON = withObject "WebSocketMessage" $ \o -> do
            mType <- o .: "type"
            mEnvId <- o .:? "envelope_id"
            mPayload <- o .:? "payload"
            mDebugInfo <- o .:? "debug_info"
            return $ WebSocketMessage mType mEnvId mPayload mDebugInfo

    data WSClientSignal = Continue | Stop (Either String ())

    class MessageReciver a where
        handleMessage :: a -> SlackMessage -> IO (Either String ())

---

    wsmAck :: T.Text -> Maybe Value -> B.ByteString
    wsmAck eid Nothing = BL.toStrict . encode $ object [
        "envelope_id" .= eid ]
    wsmAck eid (Just payload) = BL.toStrict . encode $ object [
        "envelope_id" .= eid,
        "payload" .= payload ]

    orElse :: Maybe b -> a -> Either a b
    orElse (Just n) _ = Right n
    orElse Nothing x = Left x

--- new methods vvvvv

    messageReciverClient :: (MessageReciver r) => r -> WS.ClientApp ()
    messageReciverClient re conn = let 
        open = Log.info "Hello!\n"
        close = do
            Log.info "Gracefully disconnecting...\n"
            WS.sendClose conn ("Exit" :: B.ByteString )
        in bracket_ open close $ do
            Log.info "Connected!\n"
            res <- _msgRecieverLoop re conn

            case res of
                Left err -> print err
                Right _ ->  return ()

    runClient :: WS.ClientApp () -> WebSocketUrl -> IO ()
    runClient cli (WebSocketUrl ws_host ws_path ws_port) = withSocketsDo $ runSecureClient ws_host 443 ws_path cli

    _msgRecieverLoop :: (MessageReciver r) => r -> WS.Connection -> IO (Either String ())
    _msgRecieverLoop re conn = do
        msg <- WS.receiveData conn

        Log.debug "Recieved message from Slack: %s\n" (show msg)

        signal <- case eitherDecode msg of
            Left err -> do
                Log.err "Error unmarshalling socket value: %s\n" err
                return Continue -- signal continue
            Right (HelloMessage hm) -> do
                Log.info "This bot has %d seconds to live.\n" (sh_approx_connection_time hm)
                return Continue
            Right DisconnectMessage -> do
                Log.info "Slack said goodbye!\n"
                return $ Stop (Right ())
            Right (EventMessage eid event) -> do
                Log.debug "Recieved event!\n"
                payload <- _wsEventHandle re event
                        
                let ack = wsmAck eid payload
                Log.debug "Sending ACK: %s\n" (show ack)
                WS.sendTextData conn ack
                return Continue

            Right (OtherMessage _) -> do
                Log.warn "Unsupported message\n"
                return Continue

        case signal of
            Continue -> _msgRecieverLoop re conn
            Stop outcome -> return outcome 

    _wsEventHandle :: (MessageReciver r) => r -> SlackEvent -> IO (Maybe Value)
    _wsEventHandle re (Message sm) = do
        Log.info " --- User %s said \"%s\" on channel %s\n" (sm_user sm) (sm_text sm) (sm_channel sm)
        handlerOut <- handleMessage re sm
        case handlerOut of
            Left err -> do
                Log.err "Handler error: %s" err
                return Nothing
            Right _ ->
                return Nothing
    _wsEventHandle _ (OtherEvent _) = do
        Log.warn "Other event type.\n"
        return Nothing