{-# LANGUAGE OverloadedStrings #-}

module Slack.Types where

    import Data.Aeson
    import Data.Aeson.Types (Parser)

    import qualified Data.Text as T

    type SlackEnvelopeId = T.Text

    data SlackMessage = SlackMessage {
        sm_text :: T.Text
        , sm_user :: T.Text 
        , sm_channel :: T.Text
        , sm_channel_type :: T.Text
    } deriving (Show)
    instance FromJSON SlackMessage where
        parseJSON = withObject "SlackMessage" $ \o -> do
            mtext <- o .: "text"
            muser <- o .: "user"
            mchannel <- o .: "channel"
            mchanneltype <- o .: "channel_type"
            return $ SlackMessage mtext muser mchannel mchanneltype 

    data SlackHelloInfo = SlackHelloInfo {
        sh_app_id :: T.Text,
        sh_num_connections :: Int,
        sh_approx_connection_time :: Int
    }
    instance FromJSON SlackHelloInfo where
        parseJSON = withObject "SlackHelloInfo" $ \o -> do
            connInfo <- o .: "connection_info"
            debugInfo <- o .: "debug_info"

            mAppId <- connInfo .: "app_id"
            mApproxConnTime <- debugInfo .: "approximate_connection_time"
            mNumConns <- o .: "num_connections"
            return $ SlackHelloInfo mAppId mNumConns mApproxConnTime

    data SlackEvent = 
        Message SlackMessage
        | OtherEvent Value

    instance FromJSON SlackEvent where
        parseJSON = withObject "SlackEvent" $ \o -> do
            payload <- o .: "payload"
            event <- payload .: "event"
            etype <- (event .: "type") :: Parser T.Text
            case etype of
                "message" -> Message <$> parseJSON (Object event)
                _ -> return . OtherEvent . Object $ event

    data SlackWebSocketMessage =
          DisconnectMessage 
        | HelloMessage SlackHelloInfo
        | EventMessage SlackEnvelopeId SlackEvent 
        | OtherMessage Value 

    instance FromJSON SlackWebSocketMessage where
        parseJSON = withObject "SlackWebSocketMessage" $ \o -> do
            mtype <- (o .: "type") :: Parser T.Text
            case mtype of
                "disconnect" -> return DisconnectMessage
                "hello" -> HelloMessage <$> parseJSON (Object o)
                "events_api" -> do
                    eid <- o .: "envelope_id"
                    EventMessage eid <$> parseJSON (Object o)
                _ -> return $ OtherMessage (Object o)