FROM haskell:8.10.2 as build

RUN mkdir /work
WORKDIR /work

RUN stack config set system-ghc --global true

ADD stack.yaml /work/stack.yaml
ADD stack.yaml.lock /work/stack.yaml.lock
ADD package.yaml /work/package.yaml
ADD punbot.cabal /work/punbot.cabal

RUN stack update --no-terminal 
RUN stack build --only-dependencies --no-terminal

ADD README.md /work/README.md
ADD ChangeLog.md /work/ChangeLog.md
ADD app /work/app
ADD src /work/src
ADD test /work/test
ADD Setup.hs /work/Setup.hs

RUN mkdir out
RUN stack install --local-bin-path out

########

FROM debian:buster-slim

RUN apt update && apt install -y curl

WORKDIR /app

COPY --from=build /work/out/punbot-exe /app/punbot

ADD data /app/data

ENTRYPOINT [ "/app/punbot" ]