module Main where

import Puns as P

main :: IO ()
main = do
    puns <- P.createDb "data/fortunes"
    
    pun <- P.getPunIO puns
    print $ show pun

    pun <- P.getPunIO puns
    print $ show pun

    pun <- P.getPunIO puns
    print $ show pun